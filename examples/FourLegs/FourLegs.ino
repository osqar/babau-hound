#include <BaBauHound.h>

void LabViewProtocol();

#define HipP 6
#define KneeP 10
#define AHipCorrect 60
#define AKneeCorrect 180
#define BHipCorrect 14
#define BKneeCorrect 115
#define CHipCorrect 18
#define CKneeCorrect 25
#define DHipCorrect 48
#define DKneeCorrect 155
#define Velocity 0.075 //Max 0.3 º/mseg
#define Rx 60
#define Height 20
#define MaxHeight 140


Leg LegD,LegB,LegA,LegC;

void setup()
{
  Serial.begin(57600);
  Serial.print("\n%Enter any number");  
  while(Serial.available()<=0)
  {
  }
  LegD.Create(HipP,KneeP,DHipCorrect,DKneeCorrect);
  LegD.InvertHip=False;
  LegD.InvertKnee=False;
  
  LegB.Create(HipP+1,KneeP+1,BHipCorrect,BKneeCorrect);
  LegB.InvertHip=True;
  LegB.InvertKnee=False;
  
  LegA.Create(HipP+2,KneeP+2,AHipCorrect,AKneeCorrect);
  LegA.InvertHip=False;
  LegA.InvertKnee=True;
  
  LegC.Create(HipP+3,KneeP+3,CHipCorrect,CKneeCorrect);
  LegC.InvertHip=False;
  LegC.InvertKnee=True;
  
  LegA.Delete();
  LegB.Delete();
  LegC.Delete();
}

void loop()
{
  if(Serial.available()>0)
  {
    int Cabecera;
    Cabecera=Serial.parseInt();
    switch (Cabecera)
    {
      case 255:
         float DHipAngle,DKneeAngle,BHipAngle,BKneeAngle,CHipAngle,CKneeAngle,AHipAngle,AKneeAngle;
//         AHipAngle=Serial.parseInt();
//         AKneeAngle=Serial.parseInt();         
//         BHipAngle=Serial.parseInt();
//         BKneeAngle=Serial.parseInt();
//         CHipAngle=Serial.parseInt();
//         CKneeAngle=Serial.parseInt();         
         DHipAngle=Serial.parseInt();
//         DKneeAngle=Serial.parseInt();
//         LegA.MoveHip(AHipAngle);
//         LegA.MoveKnee(AKneeAngle);         		 
//         LegB.MoveHip(BHipAngle);
//         LegB.MoveKnee(BKneeAngle);
//         LegC.MoveHip(CHipAngle);
//         LegC.MoveKnee(CKneeAngle);         
         LegD.MoveHip(DHipAngle);
//         LegD.MoveKnee(DKneeAngle);         
      break;
      case 252:
        float APositionX,APositionY,BPositionX,BPositionY,CPositionX,CPositionY,DPositionX,DPositionY;
        APositionX=Serial.parseInt();
        APositionY=Serial.parseInt();
        BPositionX=Serial.parseInt();
        BPositionY=Serial.parseInt(); 
        CPositionX=Serial.parseInt();
        CPositionY=Serial.parseInt();         
        DPositionX=Serial.parseInt();
        DPositionY=Serial.parseInt();
		
        LegA.NewMovement(APositionX,APositionY,Velocity,2*Velocity,ElbowDown);        
        LegB.NewMovement(BPositionX,BPositionY,Velocity,2*Velocity,ElbowDown);
        LegC.NewMovement(CPositionX,CPositionY,Velocity,2*Velocity,ElbowUp);
        LegD.NewMovement(DPositionX,DPositionY,Velocity,2*Velocity,ElbowUp);        

//        //NumberOfSamples
//        Serial.print("\nNumberOfSamples=");
//        Serial.print(NumberOfSamples,DEC);
//        
              
        //XInitialPosition A
        Serial.print(";\nAInitialX=");
        Serial.print(LegA.ReadPositionX(),DEC);
        
        //YInitialPosition A
        Serial.print(";\nAInitialY=");
        Serial.print(LegA.ReadPositionY(),DEC);        
        
        //XFinalPosition A
        Serial.print(";\nAFinalX=");
        Serial.print(APositionX,DEC); 

        //XInitialPosition B
        Serial.print(";\nBInitialX=");
        Serial.print(LegB.ReadPositionX(),DEC);
        
        //YInitialPosition B
        Serial.print(";\nBInitialY=");
        Serial.print(LegB.ReadPositionY(),DEC);        
        
        //XFinalPosition B
        Serial.print(";\nBFinalX=");
        Serial.print(BPositionX,DEC);         
        
        //XInitialPosition C
        Serial.print(";\nCInitialX=");
        Serial.print(LegC.ReadPositionX(),DEC);
        
        //YInitialPosition C
        Serial.print(";\nCInitialY=");
        Serial.print(LegC.ReadPositionY(),DEC);        
        
        //XFinalPosition C
        Serial.print(";\nCFinalX=");
        Serial.print(CPositionX,DEC);

        //XInitialPosition D
        Serial.print(";\nDInitialX=");
        Serial.print(LegD.ReadPositionX(),DEC);
        
        //YInitialPosition D
        Serial.print(";\nDInitialY=");
        Serial.print(LegD.ReadPositionY(),DEC);        
        
        //XFinalPosition D
        Serial.print(";\nDFinalX=");
        Serial.print(DPositionX,DEC);         
        
        while(!LegA.FinishedMovement()||!LegB.FinishedMovement()||!LegC.FinishedMovement()||!LegD.FinishedMovement())
        {
          if(!LegA.FinishedMovement())
            LegA.Move();           
          if(!LegB.FinishedMovement())
            LegB.Move();            
          if(!LegC.FinishedMovement())
            LegC.Move();
          if(!LegD.FinishedMovement())
            LegD.Move();            
        }
      break;
      case 251:
        //RECTANGULAR MOVEMENT
        LegD.Homming(ElbowUp);
        LegC.Homming(ElbowUp);        
        LegB.Homming(ElbowDown);
        LegA.Homming(ElbowDown);
        
        
        LegA.NewMovement(-Rx/2,MaxHeight,Velocity,2*Velocity,ElbowDown);        
        LegB.NewMovement(-Rx/2,MaxHeight,Velocity,2*Velocity,ElbowDown);
        LegC.NewMovement(-Rx/2,MaxHeight,Velocity,2*Velocity,ElbowUp);        
        LegD.NewMovement(-Rx/2,MaxHeight,Velocity,2*Velocity,ElbowUp);
        while(!LegD.FinishedMovement()||!LegC.FinishedMovement()||!LegB.FinishedMovement()||!LegA.FinishedMovement())
        {
          if(!LegA.FinishedMovement())
            LegA.Move();           
          if(!LegB.FinishedMovement())
            LegB.Move();            
          if(!LegD.FinishedMovement())
            LegD.Move();
          if(!LegC.FinishedMovement())
            LegC.Move();            
        }
        
        LegA.NewMovement(-Rx/2,MaxHeight-Height,Velocity,2*Velocity,ElbowDown);
        LegB.NewMovement(-Rx/2,MaxHeight-Height,Velocity,2*Velocity,ElbowDown);
        LegC.NewMovement(-Rx/2,MaxHeight-Height,Velocity,2*Velocity,ElbowUp);        
        LegD.NewMovement(-Rx/2,MaxHeight-Height,Velocity,2*Velocity,ElbowUp);
        while(!LegD.FinishedMovement()||!LegC.FinishedMovement()||!LegB.FinishedMovement()||!LegA.FinishedMovement())
        {
          if(!LegA.FinishedMovement())
            LegA.Move();           
          if(!LegB.FinishedMovement())
            LegB.Move();            
          if(!LegD.FinishedMovement())
            LegD.Move();
          if(!LegC.FinishedMovement())
            LegC.Move();            
        }
        
        LegA.NewMovement(Rx/2,MaxHeight-Height,Velocity,2*Velocity,ElbowDown);
        LegB.NewMovement(Rx/2,MaxHeight-Height,Velocity,2*Velocity,ElbowDown);
        LegC.NewMovement(Rx/2,MaxHeight-Height,Velocity,2*Velocity,ElbowUp);
        LegD.NewMovement(Rx/2,MaxHeight-Height,Velocity,2*Velocity,ElbowUp);
        while(!LegD.FinishedMovement()||!LegC.FinishedMovement()||!LegB.FinishedMovement()||!LegA.FinishedMovement())
        {
          if(!LegA.FinishedMovement())
            LegA.Move();           
          if(!LegB.FinishedMovement())
            LegB.Move();            
          if(!LegD.FinishedMovement())
            LegD.Move();
          if(!LegC.FinishedMovement())
            LegC.Move();            
        }
        
        LegA.NewMovement(Rx/2,MaxHeight,Velocity,2*Velocity,ElbowDown);
        LegB.NewMovement(Rx/2,MaxHeight,Velocity,2*Velocity,ElbowDown);
        LegC.NewMovement(Rx/2,MaxHeight,Velocity,2*Velocity,ElbowUp);
        LegD.NewMovement(Rx/2,MaxHeight,Velocity,2*Velocity,ElbowUp);
        while(!LegD.FinishedMovement()||!LegC.FinishedMovement()||!LegB.FinishedMovement()||!LegA.FinishedMovement())
        {
          if(!LegA.FinishedMovement())
            LegA.Move();           
          if(!LegB.FinishedMovement())
            LegB.Move();            
          if(!LegD.FinishedMovement())
            LegD.Move();
          if(!LegC.FinishedMovement())
            LegC.Move();            
        }
        
        LegA.NewMovement(0,MaxHeight,Velocity,2*Velocity,ElbowDown);
        LegB.NewMovement(0,MaxHeight,Velocity,2*Velocity,ElbowDown);
        LegC.NewMovement(0,MaxHeight,Velocity,2*Velocity,ElbowUp);
        LegD.NewMovement(0,MaxHeight,Velocity,2*Velocity,ElbowUp);
        while(!LegD.FinishedMovement()||!LegC.FinishedMovement()||!LegB.FinishedMovement()||!LegA.FinishedMovement())
        {
          if(!LegA.FinishedMovement())
            LegA.Move();           
          if(!LegB.FinishedMovement())
            LegB.Move();            
          if(!LegD.FinishedMovement())
            LegD.Move();
          if(!LegC.FinishedMovement())
            LegC.Move();            
        }
      break;
      case 250:
      LegB.Homming(ElbowDown);
      LegD.Homming(ElbowUp);
      for(int i=0;i<10;i++)
      {
      //Resets every time
          LegB.Delete();
          LegB.Create(HipP+1,KneeP+1,BHipCorrect,BKneeCorrect);
          LegB.InvertHip=True;
          LegB.InvertKnee=False;
     //*********************************// 
     //RectangularMovement() LegB;
        LegB.NewMovement(30,140,Velocity,2*Velocity,ElbowDown);        
        while(!LegD.FinishedMovement()||!LegB.FinishedMovement())
        {
            LegB.Move();            
        }
        
        LegB.NewMovement(30,120,Velocity,2*Velocity,ElbowDown);        
        while(!LegB.FinishedMovement())
        {
            LegB.Move();            
        }
        
        LegB.NewMovement(-30,120,Velocity,2*Velocity,ElbowDown);
        while(!LegB.FinishedMovement())
        {
            LegB.Move();            
        }
        
        LegB.NewMovement(-30,140,Velocity,2*Velocity,ElbowDown);
        while(!LegB.FinishedMovement())
        {
            LegB.Move();            
        }
        
        LegB.NewMovement(0,140,Velocity,2*Velocity,ElbowDown);
        while(!LegB.FinishedMovement())
        {
            LegB.Move();            
        }
        
     //Resets every time     
          LegD.Delete();
          LegD.Create(HipP,KneeP,DHipCorrect,DKneeCorrect);
          LegD.InvertHip=True;
          LegD.InvertKnee=False;
      //********************************//
      //RectangularMovement() LegD;       
        LegD.NewMovement(30,140,Velocity,2*Velocity,ElbowUp);
        while(!LegD.FinishedMovement())
        {
            LegD.Move();
        }
        
        LegD.NewMovement(30,120,Velocity,2*Velocity,ElbowUp);
        while(!LegD.FinishedMovement())
        {
            LegD.Move();
        }

        LegD.NewMovement(-30,120,Velocity,2*Velocity,ElbowUp);
        while(!LegD.FinishedMovement())
        {
            LegD.Move();
        }

        LegD.NewMovement(-30,140,Velocity,2*Velocity,ElbowUp);
        while(!LegD.FinishedMovement())
        {
            LegD.Move();
        }
        
        LegD.NewMovement(0,140,Velocity,2*Velocity,ElbowUp);
        while(!LegD.FinishedMovement())
        {
            LegD.Move();
        }
    }        
      break;
      case 249:
        
      break;
      case 241:
        LegD.Homming(ElbowUp);
        LegC.Homming(ElbowUp);        
        LegB.Homming(ElbowDown);
        LegA.Homming(ElbowDown);
      break;
      case 240:
		// char Select=Serial.parseInt();
        if(LegB.IsActive())
          LegB.Delete();
//        if(!LegB.IsActive())
        else
        {
          LegB.Create(HipP+1,KneeP+1,BHipCorrect,BKneeCorrect);
          LegB.InvertHip=True;
          LegB.InvertKnee=False;        
        }
      break;
      case 239:
		// char Select=Serial.parseInt();
        if(LegD.IsActive())
          LegD.Delete();
//        if(!LegB.IsActive())
        else
        {
          LegD.Create(HipP,KneeP,DHipCorrect,DKneeCorrect);
          LegD.InvertHip=True;
          LegD.InvertKnee=False;            
        }
      break;      
      default:
        Serial.parseInt();
        Serial.print(-1,DEC);
    }
    Serial.print(255,DEC);
//    Serial.print(";");
//    Serial.print("\n%Hip A\n%");
//    Serial.print(LegA.ReadHip(),DEC);
//    Serial.print("\n%Knee A\n%");    
//    Serial.print(LegA.ReadKnee(),DEC);    
//    Serial.print("\n%Hip B\n%");
//    Serial.print(LegB.ReadHip(),DEC);
//    Serial.print("\n%Knee B\n%");    
//    Serial.print(LegB.ReadKnee(),DEC);
//    Serial.print("\n%Hip C\n%");
//    Serial.print(LegC.ReadHip(),DEC);
//    Serial.print("\n%Knee C\n%");    
//    Serial.print(LegC.ReadKnee(),DEC);    
    Serial.print("\n%Hip D Angle: %");
    Serial.print(LegD.ReadHip(),DEC);
    Serial.print("\nHip D Microseconds: ");
    Serial.print(LegD.ReadHipMicroseconds(),DEC);
//    Serial.print("\n%Knee D\n%");    
//    Serial.print(LegD.ReadKnee(),DEC);
//    
//    
//    Serial.print("\n%Hip A\n%");
//    Serial.print(LegA.ReadPositionX(),DEC);
//    Serial.print("\n%Knee A\n%");    
//    Serial.print(LegA.ReadPositionY(),DEC);    
//    Serial.print("\n%Hip B\n%");
//    Serial.print(LegB.ReadPositionX(),DEC);
//    Serial.print("\n%Knee B\n%");    
//    Serial.print(LegB.ReadPositionY(),DEC);
//    Serial.print("\n%Hip C\n%");
//    Serial.print(LegC.ReadPositionX(),DEC);
//    Serial.print("\n%Knee C\n%");    
//    Serial.print(LegC.ReadPositionY(),DEC);    
//    Serial.print("\n%Hip D\n%");
//    Serial.print(LegD.ReadPositionX(),DEC);
//    Serial.print("\n%Knee D\n%");    
//    Serial.print(LegD.ReadPositionY(),DEC);    	
  }   
}
