#include <BaBauHound.h>
#include <DifferentialADC.h>
#include <Queu.h>
#include <LabViewProtocol.h>

#define HipP 6
#define KneeP 10
#define AHipCorrect 0
#define AKneeCorrect 0
#define BHipCorrect 0
#define BKneeCorrect 0
#define CHipCorrect 0
#define CKneeCorrect 0
#define DHipCorrect 0
#define DKneeCorrect 0
#define Velocity 0.0075 //Max 0.3 º/mseg
#define Rx 60
#define Height 20
#define MaxHeight 140

#define MIN_PULSETIME 800
#define MAX_PULSETIME 2200
#define MinAngle 0
#define MaxAngle


ADCManager ADManager;
DifferentialADC ADC1,ADC2;

Leg LegD,LegB,LegA,LegC;
LegMovementManager LegManager;
byte i=0;
long LastTime=0;
bool Initialize=TRUE;
bool Rectangular=FALSE;

#define SampleTime 120

//TEST: just for reed
TrajectoryPlanner * LegDTraject;
Queu SQueu;

void setup()
{
  ADManager.Initialize();
  ADC1.Attach(&ADManager);
  
  Serial.begin(57600);  
  SerialInitialize(&SQueu);
  
  byte Status=ADC2.Attach(&ADManager);
  
  LegD.Create(HipP,KneeP,DHipCorrect,DKneeCorrect,LegManager);
  LegD.InvertHip=False;
  LegD.InvertKnee=False;
  
  LegD.Homming(ElbowDown);
  LegDTraject=&LegD.Traject;
  
  Serial.print("\nCurrentHip: ");
//  Serial.print(LegD.ReadPositionX(),DEC);
//  
//  Serial.print("\nCurrentKnee: ");
//  Serial.print(LegD.ReadPositionY(),DEC);

  LegD.NewMovement(-20,135,Velocity,2*Velocity,ElbowDown);  
}

void loop()
{
       float DPositionX,DPositionY;
        int Angle;
        ADManager.Runner();                  
        
        if(!Rectangular)
        {
          if(i%2==0)
          {
            if(Initialize)
            {
              DPositionX=-20;
              DPositionY=135;	
              LegD.NewMovement(DPositionX,DPositionY,Velocity,2*Velocity,ElbowDown);
              Initialize=FALSE;
            }
            Angle=120;        
          }
          else
          {
            if(Initialize)
            {
              DPositionX=-115;
              DPositionY=20;		
              LegD.NewMovement(DPositionX,DPositionY,Velocity,2*Velocity,ElbowDown);
              Initialize=FALSE;
            }
            Angle=60;         
          }    
        }
                      
        if(Rectangular)
        {
          //RECTANGULAR MOVEMENT
           if(i==0)
           {       
            if(Initialize)
            {
              LegD.NewMovement(-Rx/2,MaxHeight,Velocity,2*Velocity,ElbowDown);
              Initialize=FALSE;
            }
           }
          
           if(i==1)
           { 
            if(Initialize)
            {
              LegD.NewMovement(-Rx/2,MaxHeight-Height,Velocity,2*Velocity,ElbowDown);
              Initialize=FALSE;
            }
           }
            
           if(i==2)
           { 
            if(Initialize)
            {
              LegD.NewMovement(Rx/2,MaxHeight-Height,Velocity,2*Velocity,ElbowDown);
              Initialize=FALSE;
            }
           }
            
           if(i==3)
           { 
            if(Initialize)
            {
              LegD.NewMovement(Rx/2,MaxHeight,Velocity,2*Velocity,ElbowDown);
              Initialize=FALSE;
            }
           }
  
           if(i==4)
           { 
            if(Initialize)
            {
              LegD.NewMovement(0,MaxHeight,Velocity,2*Velocity,ElbowDown);
              Initialize=FALSE;
            }
           }
        }
        
        
        long ActualTime=millis();
        if(ActualTime-LastTime>SampleTime)
        {
          LastTime=millis();
          SerialSubscribe(ADC1.ReadConversion()/2,1);
          SerialSubscribe(LegDTraject->CurrentTrackHip,2);
          SerialSubscribe(ADC2.ReadConversion()/2,3);
          SerialSubscribe(LegDTraject->CurrentTrackKnee,4);
          SerialSubscribe(LegDTraject->FinalTrackHip,5,LegDTraject->FinalTrackKnee);
//          SerialSubscribe(LegDTraject->FinalTrackKnee,6);
//          SerialSubscribe(LegDTraject->InitialPositionX,7);
//          SerialSubscribe(LegDTraject->InitialPositionY,8);
//          SerialSubscribe(LegDTraject->FinalPositionX,9);
//          SerialSubscribe(LegDTraject->FinalPositionY,10);
          //if you want to add a sample, put it in the parameters and resize SerialSample buffer, and definition
          SerialSample();
        }
        
        if(LegD.FinishedMovement())
        {
          Initialize=TRUE;
          i++;
          if(i>2&&!Rectangular)
          {
            Rectangular=TRUE;
            i=0;
          }
          if(i>4)
          {
            Rectangular=FALSE;
            i=0;
          }
        }
        
        if(!LegD.FinishedMovement())
        {
            LegD.Move();           
        }               
}