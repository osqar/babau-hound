#include <BaBauHound.h>
#include <TrajectoryPlanner.h>
#include <Queu.h>

#define HipP 6
#define KneeP 10
#define DHipCorrect 0
#define DKneeCorrect 0

Leg LegD;

#define MoveRate 2000
float LastTime,ActualTime;
float HipAngle;
int i=0;

void setup()
{
  LegD.Create(HipP,KneeP,DHipCorrect,DKneeCorrect);
  LegD.InvertHip=False;
  LegD.InvertKnee=False;
  
  LastTime=0;
}

void loop()
{
  ActualTime=millis();
  if(ActualTime-LastTime>=MoveRate)
  {
    LastTime=millis();
    switch(i%2)
    {
      case 0:
        HipAngle=-20;
      break;
      case 1:
        HipAngle=20;      
      break;
      default:
        HipAngle=-20;      
    }
    LegD.MoveHip(LegD.ReadHip()+HipAngle);
    i++;
  }
}